# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/web/ssl-proxy/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/web/ssl-proxy/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/web/ssl-proxy/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/web/ssl-proxy/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/web/ssl-proxy?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/web/ssl-proxy

[docker-pull-image]: https://img.shields.io/docker/pulls/mydcs/ssl-proxy.svg
[docker-pull-link]: https://hub.docker.com/r/mydcs/ssl-proxy
[docker-release-image]: https://img.shields.io/docker/v/mydcs/ssl-proxy?sort=semver
[docker-release-link]: https://hub.docker.com/r/mydcs/ssl-proxy
[docker-stars-image]: https://img.shields.io/docker/stars/mydcs/ssl-proxy.svg
[docker-stars-link]: https://hub.docker.com/r/mydcs/ssl-proxy
[docker-size-image]: https://img.shields.io/docker/image-size/mydcs/ssl-proxy/latest.svg
[docker-size-link]: https://hub.docker.com/r/mydcs/ssl-proxy


[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]


[![docker-pull-image]][docker-pull-link] 
[![docker-release-image]][docker-release-link]
[![docker-stars-image]][docker-stars-link]
[![docker-size-image]][docker-size-link]

# How To

## Vorbereitung

1. Arbeitsverzeichnis anlegen
2. In das Arbeitsverzeichnis wechseln
3. Unterordner config anlegen
4. Datei domains.dat im Ordner config anlegen
5. Container starten, entweder ueber "docker run" oder "docker compose"

## Aufruf

### CLI

```
docker run -it --rm -e DOMAIN=example -e TLD=com -v $(pwd)/config:/srv -v /etc/letsencrypt:/etc/letsencrypt mydcs/ssl_proxy:latest
```

### Compose

```dockerfile
version: "3.8"

services:
  ssl_proxy:
    container_name: ssl_proxy
    hostname: ssl_proxy
    restart: always
    image: mydcs/ssl_proxy:latest
    environment:
      - DOMAIN=${DOMAIN:-example}
      - TLD=${TLD:-com}
    networks:
      - dcs
    ports:
      - 443:443
    volumes:
      - $PWD/config:/srv
      - /etc/letsencrypt:/etc/letsencrypt

networks:
  dcs:
    name: dcs
```

## Generelle SSL Adressen

Um das Standard-Template fuer SSL zu nutzen einfache eine Datei domains.dat anlegen.
Der Aufbau ist wie folgt:
```
# Kommentar
Domain	Ziel # weitere Parameter werden nicht interpretiert
```

Folgendes Beispiel illustriert den einfachen Aufbau:
```
nextcloud.example.com	http://raspberry-pi.fritz.box:8080
pma.example.com		http://pi-3.fritz.box:80 		# PHP My Admin
```

## Spezielle SSL Config

Fuer spezielle Setup ist es moeglich eine Datei mit der Endung ".conf" anzulegen. Diese wird entsprechend ebenfalls beruecksichtigt.

Hier ein Beispiel fuer Nextcloud und Only-Office, beide sind auf unterschiedlichen VMs installiert

```
server {
    listen                      443 ssl http2;
    server_name                 nextcloud.example.com;
    access_log                  /var/log/nginx/nextcloud.example.com-ssl-access_log;
    error_log                   /var/log/nginx/nextcloud.example.com-ssl-error_log;

    root /var/www/nextcloud.example.com;
    index index.html index.htm index.php;

    
    # Only-Office auf separater VM
    location ~* ^/ds-vpath/ {
        rewrite /ds-vpath/(.*) /$1  break;
        proxy_pass http://192.168.178.18;
        proxy_redirect     off;

        client_max_body_size 100m;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Host $host/ds-vpath;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    # Nextcloud auf separater VM
    location / {
        proxy_pass https://192.168.178.17:443;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-for $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Proto $remote_addr;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_redirect off;

        # Send websocket data to the backend aswell
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
    }

}
```
