#!/bin/bash

export NAME=$(basename $PWD)
export TAG=latest

docker build -t $NAME:$TAG .

docker run -it --rm \
    $* \
    -p 443:443 \
    --network dcs \
    --hostname $NAME \
    $NAME:$TAG

#docker image prune -a -f
