FROM	alpine:3.17
LABEL	maintainer=Dis4sterRec0very

#
# Setup environment
#
ENV	DOMAIN=${DOMAIN:-example}
ENV	TLD=${TLD:-com}

RUN apk --no-cache add \
	htop \
	gettext \
	openssl \
	nginx 

# files to image the easy way
COPY img_fs /

RUN chmod 777 /srv

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 443/tcp
