#!/bin/sh

DOMAIN=${DOMAIN:-example}
TLD=${TLD:-com}

export DOLLAR="$"

cat /srv/domains.dat | grep -v '^#' | while read NAME URI
do
    export NAME URI
    envsubst < /etc/nginx/site_template.conf > /etc/nginx/http.d/$NAME.$DOMAIN.$TLD.conf
    cat  /etc/nginx/http.d/$NAME.$DOMAIN.$TLD.conf
done

nginx -g 'daemon off;'